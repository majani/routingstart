import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router,
    private authLog:AuthService
  ) { }

  ngOnInit() {
  }

  onLoadServers(id:number){
    this.router.navigate(['/servers', id,'edit'], {queryParams:{allowEdit:'1'}, fragment:'loading'})
  }

  logIn(){
    this.authLog.logIn();

  }

  logOut(){
    this.authLog.logOut();

  }

}
