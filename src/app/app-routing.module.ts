import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { UserComponent } from './users/user/user/user.component';
import { ServersComponent } from './servers/servers/servers.component';
import { UsersComponent } from './users/users/users.component';
import { EditServerComponent } from './servers/edit-server/edit-server/edit-server.component';
import { ServerComponent } from './servers/server/server/server.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AuthGuard } from './auth-guard.service';
import { CanDeactivateGuard } from './servers/edit-server/edit-server/can-deactivate-guard.service';
import { ErrorpageComponent } from './errorpage/errorpage.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'users',
    component: UsersComponent,
    children:
      [
        { path: ':id/:name', component: UserComponent },
      ]
  },
  {
    path: 'servers',
    component: ServersComponent,
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children:
      [
        { path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] },
        { path: ':id', component: ServerComponent }
      ]
  },
  // {
  //   path: 'not-found', component:PagenotfoundComponent
  // },
  {
    path: 'not-found', component: ErrorpageComponent, data: {message: 'Page Not Found'}
  },
  {
    path: '**',
    redirectTo: '/not-found'
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
